package wolfgoatcabbage;

import ilog.concert.IloException;

/**
 * Solves the Wolf-Goat-Cabbage logic puzzle.
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class WolfGoatCabbage {

  /**
   * Dummy constructor.
   */
  private WolfGoatCabbage() { }

  /**
   * Solves the puzzle.
   * @param args the command line arguments (ignored)
   */
  public static void main(final String[] args) {
    System.out.println("Applying the MIP model.");
    try (MIP model = new MIP()) {
      double z = model.solve();
      System.out.println("Optimal number of trips = " + z + ".");
      System.out.println("Solution:");
      System.out.println(model.getSolution());
    } catch (IloException ex) {
      System.out.println("CPLEX exception:\n" + ex.getMessage());
    }
    System.out.println("\nApplying the CP model:");
    try (CP model = new CP()) {
      int z = model.solve();
      System.out.println("Optimal number of trips = " + z + ".");
      System.out.println("Solution:");
      System.out.println(model.getSolution());
    } catch (IloException ex) {
      System.out.println("CPOptimizer exception:\n" + ex.getMessage());
    }
  }

}
