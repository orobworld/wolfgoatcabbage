package wolfgoatcabbage;

import ilog.concert.IloException;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;
import static wolfgoatcabbage.Problem.CABBAGE;
import static wolfgoatcabbage.Problem.GOAT;
import static wolfgoatcabbage.Problem.ITEMCOUNT;
import static wolfgoatcabbage.Problem.TMAX;
import static wolfgoatcabbage.Problem.WOLF;

/**
 * MIP provides a mixed integer linear programming model to solve the logic
 * puzzle.
 *
 * The model captures trips between the "near" bank (starting point) and
 * "far" bank (destination). At time 0, the farmer is on the near bank with
 * three items (wolf, goat, cabbage). By the ending time (TMAX) the farmer
 * must be on the far bank with all three items.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class MIP implements AutoCloseable {

  // CPLEX objects.
  private final IloCplex cplex;               // the model instance
  private final IloNumVar[][] inventoryNear;  // item inventories on near bank
  private final IloNumVar[][] inventoryFar;   // item inventories on far bank
  private final IloNumVar[][] nearToFar;      // items carried near to far
  private final IloNumVar[][] farToNear;      // items carried far to near
  private final IloNumVar[] inProgress;       // 1 if in progress, 0 if done

  /**
   * Constructor.
   * @throws IloException if the model instance cannot be created
   */
  public MIP() throws IloException {
    // Instantiate the model.
    cplex = new IloCplex();
    // Create the variables.
    inventoryNear = new IloNumVar[TMAX][ITEMCOUNT];
    inventoryFar = new IloNumVar[TMAX][ITEMCOUNT];
    nearToFar = new IloNumVar[TMAX][ITEMCOUNT];
    farToNear = new IloNumVar[TMAX][ITEMCOUNT];
    inProgress = new IloNumVar[TMAX];
    for (int t = 0; t < TMAX; t++) {
      inProgress[t] = cplex.intVar(0, 1, "inProgress_" + t);
      for (int i = 0; i < ITEMCOUNT; i++) {
        inventoryNear[t][i] = cplex.numVar(0, 1, "nearInv_" + i + "_" + t);
        inventoryFar[t][i] = cplex.numVar(0, 1, "farInv_" + i + "_" + t);
        // Movement variables are undefined at time 0.
        if (t > 0) {
          nearToFar[t][i] = cplex.intVar(0, 1, "nearToFar_" + i + "_" + t);
          farToNear[t][i] = cplex.intVar(0, 1, "farToNear_" + i + "_" + t);
        }
      }
    }
    // The objective is to minimize the number of trips, or equivalently the
    // number of time periods during which movement occurs.
    cplex.addMinimize(cplex.sum(inProgress));
    for (int i = 0; i < ITEMCOUNT; i++) {
      // We being with all inventory on the near bank.
      inventoryNear[0][i].setLB(1);
      inventoryFar[0][i].setUB(0);
    }
    // In odd numbered time periods, movement is from the near bank to the
    // far bank. In even numbered periods, movement is from the far bank
    // to the near bank. When movement occurs, at most one item moves.
    for (int t = 1; t < TMAX; t++) {
      if (t % 2 == 0) {
        // Even period; flow is far to near.
        cplex.addEq(cplex.sum(nearToFar[t]), 0);
        cplex.addLe(cplex.sum(farToNear[t]), 1);
      } else {
        // Odd period; flow is near to far.
        cplex.addEq(cplex.sum(farToNear[t]), 0);
        cplex.addLe(cplex.sum(nearToFar[t]), 1);
      }
    }
    // In each period, flows add to destination inventory and deplete
    // origin inventory.
    for (int t = 1; t < TMAX; t++) {
      for (int i = 0; i < ITEMCOUNT; i++) {
        cplex.addEq(inventoryNear[t][i],
                    cplex.sum(inventoryNear[t - 1][i],
                              cplex.diff(farToNear[t][i], nearToFar[t][i])));
        cplex.addEq(inventoryFar[t][i],
                    cplex.sum(inventoryFar[t - 1][i],
                              cplex.diff(nearToFar[t][i], farToNear[t][i])));
      }
    }
    // In an odd numbered period (where the farmer ends up on the far bank),
    // neither wolf and goat nor goat and cabbage can be on the near bank.
    for (int t = 1; t < TMAX; t += 2) {
      cplex.addLe(cplex.sum(inventoryNear[t][WOLF], inventoryNear[t][GOAT]), 1);
      cplex.addLe(cplex.sum(inventoryNear[t][GOAT], inventoryNear[t][CABBAGE]),
                  1);
    }
    // In an even numbered period (where the farmer ends up on the near bank),
    // neither wolf and goat nor goat and cabbage can be on the far bank
    // UNLESS the problem is completed (in which case the farmer remains on
    // the far bank).
    for (int t = 2; t < TMAX; t += 2) {
      cplex.addLe(cplex.sum(inventoryFar[t][WOLF], inventoryFar[t][GOAT]),
                  cplex.diff(2, inProgress[t]));
      cplex.addLe(cplex.sum(inventoryFar[t][GOAT], inventoryFar[t][CABBAGE]),
                  cplex.diff(2, inProgress[t]));
    }
    // The transfer remains in progress until nothing is left on the near bank.
    for (int t = 1; t < TMAX; t++) {
      cplex.addLe(cplex.sum(inventoryNear[t - 1]),
                            cplex.prod(ITEMCOUNT, inProgress[t]));
    }
    // Suppress solver output.
    cplex.setOut(null);
  }

  /**
   * Solves the model.
   * @return the optimal objective value
   * @throws IloException if CPLEX blows up
   */
  public double solve() throws IloException {
    cplex.solve();
    return cplex.getObjValue();
  }

  /**
   * Closes the model.
   */
  @Override
  public void close() {
    cplex.close();
  }

  /**
   * Gets the solution to the puzzle.
   * @return a string displaying the solution
   * @throws IloException if the solution does not exist
   */
  public String getSolution() throws IloException {
    double obj = cplex.getObjValue();
    int nTrips = (int) Math.round(obj);
    double[][] near = new double[TMAX][];
    double[][] transit = new double[TMAX][];
    double[][] far = new double[TMAX][];
    for (int t = 0; t <= nTrips; t++) {
      near[t] = cplex.getValues(inventoryNear[t]);
      far[t] = cplex.getValues(inventoryFar[t]);
      // Movement variables are undefined at time 0.
      if (t > 0) {
        if (t % 2 == 0) {
          transit[t] = cplex.getValues(farToNear[t]);
        } else {
          transit[t] = cplex.getValues(nearToFar[t]);
        }
      }
    }
    return Problem.report(obj, near, transit, far);
  }

}
