package wolfgoatcabbage;

/**
 * Problem is a static class that provides the maximum number of time periods
 * to consider and various utilities.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Problem {
  // Item indices.
  /** WOLF is the index for the wolf. */
  public static final int WOLF = 0;
  /** GOAT is the index for the goat. */
  public static final int GOAT = 1;
  /** CABBAGE is the index for the cabbage. */
  public static final int CABBAGE = 2;
  /** ITEMCOUNT is the number of distinct items in the problem. */
  public static final int ITEMCOUNT = 3;

  // Item names.
  private static final String[] ITEMS =
    new String[] {"wolf", "goat", "cabbage"};

  // Formatting information.
  private static final int WIDTH = 20;  // field width in formatted lines

  /** TMAX is the time period limit (1 greater than maximum trips allowed). */
  public static final int TMAX = 17;

  // Cutoff for rounding integers.
  private static final double HALF = 0.5;

  /**
   * Dummy constructor to prevent instantiation.
   */
  private Problem() { }

  /**
   * Generates a string summary of a solution.
   *
   * The first index in each matrix argument is time; the second is the item.
   *
   * @param obj the objective value (number of trips involved)
   * @param near the matrix of item inventories on the near bank
   * @param transit the matrix of item counts in the boat
   * @param far the matrix of item inventories on the far bank
   * @return a string summarizing the solution.
   */
  public static String report(final double obj, final double[][] near,
                              final double[][] transit, final double[][] far) {
    StringBuilder sb = new StringBuilder();
    sb.append("""
              The farmer arrives at the left bank at time 0.
              """);
    // Turn the objective value into the integer number of trips.
    int nTrips = (int) Math.round(obj);
    // Add each trip to the report.
    for (int t = 1; t <= nTrips; t++) {
      sb.append("Time ").append(t).append(":\t");
      if (t % 2 == 0) {
        // Even numbered trips are right to left.
          sb.append(format(what(near[t - 1]), what(transit[t]),
                           what(far[t]), false));
      } else {
        // Odd numbered trips are left to right.
        sb.append(format(what(near[t]), what(transit[t]),
                         what(far[t - 1]), true));
      }
      sb.append("\n");
    }
    return sb.toString();
  }

  /**
   * Generates a string summary of a solution.
   *
   * The first index in the matrix argument is time, the second is item.
   *
   * @param nTrips
   * @param far
   * @param carried
   * @return 
   */
  public static String report(final int nTrips, final double[][] far,
                              final double[] carried) {
    StringBuilder sb = new StringBuilder();
    sb.append("""
              The farmer arrives at the left bank at time 0.
              """);
    // Add each trip to the report.
    for (int t = 1; t <= nTrips; t++) {
      sb.append("Time ").append(t).append(":\t");
      String boat;
      int c = (int) Math.round(carried[t]);
      if (c < ITEMCOUNT) {
        boat = ITEMS[c];
      } else {
        boat = "nothing";
      }
      if (t % 2 == 0) {
        // Trip is far to near.
        sb.append(format(what(flip(far[t - 1])), boat, what(far[t]), false));
      } else {
        // Trip is near to far.
        sb.append(format(what(flip(far[t])), boat, what(far[t - 1]), true));
      }
      sb.append("\n");
    }
    return sb.toString();
  }

  /**
   * Gets the items (if any) indicated by a vector of 0-1 values.
   * @param x the vector of variable values
   * @return the symbols of the indicated items (if any); "nothing" if none are
   * indicated
   */
  private static String what(final double[] x) {
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < ITEMCOUNT; i++) {
      if (x[i] > HALF) {
        if (!sb.isEmpty()) {
          sb.append(", ");
        }
        sb.append(ITEMS[i]);
      }
    }
    if (sb.isEmpty()) {
      sb.append("nothing");
    }
    return sb.toString();
  }

  /**
   * Merges three strings into a formatted string describing a trip.
   * @param left the left bank inventory
   * @param center the contents of the boat
   * @param right the right bank inventory
   * @param lr true if the trip is left to right, false if right to left
   * @return a formatted string displaying the trip
   */
  private static String format(final String left, final String center,
                               final String right, final boolean lr) {
    StringBuilder sb = new StringBuilder();
    int n = left.length();
    sb.append(String.format("%" + WIDTH + "s", left));
    String m;
    if (lr) {
      m = "-- " + center + " ->";
    } else {
      m = "<- " + center + " --";
    }
    n = m.length();
    int p0 = n + (WIDTH - n) / 2;  // width including left padding
    m = String.format("%" + p0 + "s", m);
    m = String.format("%-" + WIDTH + "s", m);
    sb.append(" ").append(m).append(" ");
    n = right.length();
    sb.append(String.format("%-" + WIDTH + "s", right));
    return sb.toString();
  }

  /**
   * Reverses the 0-1 values in a vector.
   * @param x a vector of 0-1 values
   * @return the logical negation of x
   */
  private static double[] flip(final double[] x) {
    double[] y = new double[x.length];
    for (int i = 0; i < x.length; i++) {
      y[i] = 1 - x[i];
    }
    return y;
  }
}
